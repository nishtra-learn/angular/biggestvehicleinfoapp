import { Component, OnInit } from '@angular/core';
import { VehicleInfoModel } from '../shared/vehicle-info-model';
import { VehicleInfoService } from '../shared/vehicle-info.service';

@Component({
  selector: 'app-biggest-vehicle-display',
  templateUrl: './biggest-vehicle-display.component.html',
  styleUrls: ['./biggest-vehicle-display.component.css']
})
export class BiggestVehicleDisplayComponent implements OnInit {
  selectedVehicle: VehicleInfoModel;

  constructor(private vehicleInfoService: VehicleInfoService) {
    this.selectedVehicle = vehicleInfoService.biggestCar;
  }

  ngOnInit(): void {
  }

  selectBiggestCar() {
    this.selectedVehicle = this.vehicleInfoService.biggestCar;
  }

  selectBiggestPlane() {
    this.selectedVehicle = this.vehicleInfoService.biggestPlane;
  }

  selectBiggestShip() {
    this.selectedVehicle = this.vehicleInfoService.biggestShip;
  }

}
