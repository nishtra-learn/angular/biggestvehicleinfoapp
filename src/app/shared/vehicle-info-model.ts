export class VehicleInfoModel {
    title: string;
    imageUrl: string;
    description: string;
}
