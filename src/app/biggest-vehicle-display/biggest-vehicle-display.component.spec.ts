import { ComponentFixture, TestBed } from '@angular/core/testing';

import { BiggestVehicleDisplayComponent } from './biggest-vehicle-display.component';

describe('BiggestVehicleDisplayComponent', () => {
  let component: BiggestVehicleDisplayComponent;
  let fixture: ComponentFixture<BiggestVehicleDisplayComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ BiggestVehicleDisplayComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(BiggestVehicleDisplayComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
