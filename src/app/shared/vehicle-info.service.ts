import { Injectable } from '@angular/core';
import { VehicleInfoModel } from "./vehicle-info-model";

@Injectable({
  providedIn: 'root'
})
export class VehicleInfoService {
  biggestCar: VehicleInfoModel;
  biggestPlane: VehicleInfoModel;
  biggestShip: VehicleInfoModel;

  constructor() {
    this.biggestCar = new VehicleInfoModel();
    this.biggestCar.title = 'NASA-Crawler-Transporter';
    this.biggestCar.description = `In 1965, NASA developed two crawler-transporters to carry rockets to their launch pads. At the time, 
    these massive space shuttle transporters – measuring 39,929 metres long by 34,747 metres wide, and over 6,096 metres in height – were the largest 
    self-powered land vehicles in the world.
    The transporters, which are named Hans and Franz, are now used to transport shuttles between the Vehicle Assembly Building and Launch Complex 39.`;
    this.biggestCar.imageUrl = 'https://www.khplant.co.za/sites/default/files/uploads/nasa-crawler-transporter-1.jpg';

    this.biggestPlane = new VehicleInfoModel();
    this.biggestPlane.title = 'Antonov an-225 Mriya';
    this.biggestPlane.description = `This cargo plane was designed by the Soviet Union’s Antonov Design Bureau in the 1980s for strategic military airlifts. 
    The Soviets were known for building large aircraft, and the Mriya is no exception. At 83,82 metres long, with a weight of 285 tonnes, a wing span of 
    88,39 metres and a cargo space large enough to hold 50 cars, Mriya is both the largest and the heaviest aeroplane on Earth.`;
    this.biggestPlane.imageUrl = 'https://www.khplant.co.za/sites/default/files/uploads/ant';

    this.biggestShip = new VehicleInfoModel();
    this.biggestShip.title = 'Symphony Of the Seas';
    this.biggestShip.description = `The 25th ship in Royal Caribbean’s fleet, Symphony of the Seas is currently the world’s largest cruise ship. 
    The giant cruise ship has 228,081 gross registered tons, measures 238 feet tall and spans 1,188 feet long. Symphony of the seas is termed as the 
    ultimate family’s vacation destination with bold lineup of energy-filled, heart-pumping experiences. The largest cruise ship in the world 
    offers the chance to go head-to-head in a glow-in-the-dark laser tag adventure, take the plunge down the Ultimate Abyss – the tallest slide at sea 
    towering 10 stories high – or soak up some sun after getting drenched on epic waterslides and FlowRider surf simulators. 
    The biggest cruise ship was delivered in April 2018.`;
    this.biggestShip.imageUrl = 'https://imagesvc.meredithcorp.io/v3/mm/image?url=https%3A%2F%2Fstatic.onecms.io%2Fwp-content%2Fuploads%2Fsites%2F28%2F2017%2F04%2Flead-royal-caribbean-symphony-SYMPHONY0417.jpg&q=85';
  }
}
