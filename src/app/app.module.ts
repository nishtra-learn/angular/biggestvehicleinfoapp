import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';

import { AppComponent } from './app.component';
import { BiggestVehicleDisplayComponent } from './biggest-vehicle-display/biggest-vehicle-display.component';

@NgModule({
  declarations: [
    AppComponent,
    BiggestVehicleDisplayComponent
  ],
  imports: [
    BrowserModule
  ],
  providers: [],
  bootstrap: [AppComponent]
})
export class AppModule { }
